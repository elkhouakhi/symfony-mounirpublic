<?php

namespace App\DataFixtures;

use App\Entity\Continents;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;

class ContinentsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
    	$faker = Faker::create();
        $tab = array("Afrique", "Asie", "Europe", "Océanie", "Amérique");
	    for($i = 0; $i < 5; $i++) {
		    $continent = new Continents();
			$continent->setName($tab[$i]);
			$this->addReference("c$i", $continent);
		    $manager->persist($continent);
	    }

        $manager->flush();
    }
}
