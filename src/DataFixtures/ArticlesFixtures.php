<?php

namespace App\DataFixtures;

use App\Entity\Articles;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ArticlesFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
    	$faker = Faker::create();
        $img = array("Afrique.jpg", "Asie.jpg", "Europe.jpg", "Océanie.jpg", "Amérique.jpg");
	    for($i = 0; $i < 50; $i++) {
		    $article = new Articles();
		    $article->setName( $faker->sentence(3) );
			$article->setDescription( $faker->text(200) );
			$randomContinent = random_int(0, 4);
		    $article->setImage($img[$randomContinent]);
		    $continent = $this->getReference("c$randomContinent");

		    $article->setContinent($continent);

		    $manager->persist($article);
	    }

        $manager->flush();
    }
    public function getDependencies()
    {
        return [
            ContinentsFixtures::class
        ];
    }
}
