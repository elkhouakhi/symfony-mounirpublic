<?php

namespace App\EventSubscriber\Entity;



use App\Entity\Articles;
use App\Service\FileService;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ArticlesEventSubscriber implements EventSubscriber
{
	
	private $fileService;

	public function __construct(FileService $fileService)
	{
		$this->fileService = $fileService;
	}

	public function prePersist(LifecycleEventArgs $event):void
	{
		if($event->getObject() instanceof Product){
			$product = $event->getObject();

			if($product->getImage() instanceof UploadedFile){
				$this->fileService->upload( $product->getImage(), 'img/product' );

				$product->setImage( $this->fileService->getFileName() );
			}
		}
	}


	public function getSubscribedEvents()
	{
		return [
			Events::prePersist,
			Events::postLoad,
			Events::preUpdate,
			Events::preRemove
		];
	}

	public function preRemove(LifecycleEventArgs $args):void
	{
		if($args->getObject() instanceof Product) {
			$product = $args->getObject();
			$this->fileService->delete( $product->prevImage, 'img/product' );
		}
	}

	public function preUpdate(LifecycleEventArgs $args):void
	{
		if($args->getObject() instanceof Product){
			$product = $args->getObject();
			if($product->getImage() instanceof UploadedFile){
				$this->fileService->upload($product->getImage(), 'img/product');
				$product->setImage( $this->fileService->getFileName() );

				$this->fileService->delete( $product->prevImage, 'img/product' );
			}
			else {
				$product->setImage( $product->prevImage );
			}
		}
	}

	public function postLoad(LifecycleEventArgs $args):void
	{
		if($args->getObject() instanceof Product){
			$product = $args->getObject();
			$product->prevImage = $product->getImage();
		}
	}
}







