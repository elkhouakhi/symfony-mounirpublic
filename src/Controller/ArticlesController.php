<?php

namespace App\Controller;

use App\Repository\ArticlesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticlesController extends AbstractController
{
	/**
	 * @Route("/articles", name="articles.index")
	 */
	public function index(ArticlesRepository $articlesRepository):Response
	{

		$results = $articlesRepository->findAll();

		return $this->render('articles/index.html.twig', [
			'results' => $results
		]);
	}	
		
	
	/**
	 * @Route("/articles/gerer", name="articles.administrer")
	 */
	public function administrer(ArticlesRepository $articlesRepository):Response
	{

		$results = $articlesRepository->findAll();

		return $this->render('articles/gestion/index.html.twig', [
			'results' => $results
		]);
	}

		/**
	 * @Route("/articles/details/{id}", name="articles.plus")
	 */
	public function plus(string $id,ArticlesRepository $articlesRepository):Response
	{

		$article = $articlesRepository->findOneBy([
			'id' => $id
		]);

		return $this->render('articles/details.html.twig', [
			'article' => $article
		]);
	}


}


