<?php

namespace App\Controller\Admin;

use App\Entity\Articles;
use App\Form\ArticleType;
use App\Repository\ArticlesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class ArticleController extends AbstractController
{
	/**
	 * @Route("/articles/delete/{id}", name="admin.article.delete")
	 */
	public function delete(ArticlesRepository $articlesRepository, EntityManagerInterface $entityManager, int $id):Response
	{

		$entity = $articlesRepository->find($id);
		$entityManager->remove($entity);
		$entityManager->flush();

		$this->addFlash('notice', 'Le produit a été supprimé');
		return $this->redirectToRoute('articles.administrer');
	}

		/**
	 * @Route("/articles/form", name="admin.article.form")
	 * @Route("/articles/form/update/{id}", name="admin.article.form.update")
	 */
	public function form(Request $request, EntityManagerInterface $entityManager, int $id = null, ArticlesRepository $articleRepository):Response
	{
		$type = ArticleType::class;
		$model = $id ? $articleRepository->find($id) : new Articles();
		$form = $this->createForm($type, $model);
		$form->handleRequest($request);

		if($form->isSubmitted() && $form->isValid()){
			$id ? null : $entityManager->persist($model);
			$entityManager->flush();

			$message = $id ? "Le produit a été modifié" : "Le produit a été ajouté";
			$this->addFlash('notice', $message);

			return $this->redirectToRoute('articles.administrer');
		}

		return $this->render('articles/gestion/form.html.twig', [
			'form' => $form->createView()
		]);
	}
}








